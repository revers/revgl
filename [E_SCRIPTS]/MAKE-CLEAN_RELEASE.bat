@set REV_PROJECT_NAME=RevGL
@call set_workspace_path.bat
@IF %ERRORLEVEL% NEQ 0 ( 
echo ERROR: set_workspace_path.bat doesn't exist in eclipse.exe's folder!
	pause 
	exit /b 1
)

@cd %REV_WORKSPACE_PATH%
@cd %REV_PROJECT_NAME%
@cd "[SCRIPTS]"

call ant -f ../build.xml make_clean_release 

@IF %ERRORLEVEL% NEQ 0 (
	title ERROR %ERRORLEVEL%
) ELSE (
	title DONE
 exit /b 0
)
pause 
