#include <GL/glew.h>

#include <rev/gl/RevGLTextureRenderTarget.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevAssert.h>

using namespace rev;

bool GLTextureRenderTarget::createTextures() {
    if (!textureArray) {
        delete [] textureArray;
    }

    textureArray = new GLTexture2D[textures];

    for (int i = 0; i < textures; i++) {
        textureArray[i].setInitialSize(width, height);
        textureArray[i].setInitialInternalFormat(format);
        textureArray[i].setInitialSampler(sampler);

        if (!textureArray[i].create()) {
            return false;
        }
        glAssert;
    }

    currentTexIndex = 0;
    currentTexture = &textureArray[currentTexIndex];

    return true;
}

bool GLTextureRenderTarget::create(int width, int height) {
    this->textures = textures;

    destroy();

    setInitialSize(width, height);
    if (!createTextures()) {
        return false;
    }

    glGenFramebuffers(1, &fboHandle);

    if (!fboHandle) {
        return false;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, fboHandle);

    glActiveTexture(GL_TEXTURE0);
    currentTexture->bind();

    glFramebufferTexture2D(GL_FRAMEBUFFER,
            GL_COLOR_ATTACHMENT0,
            GL_TEXTURE_2D,
            currentTexture->handle(), 0);

    GLenum drawBuffers[] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, drawBuffers);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    currentTexture->unbind();

    return true;
}

GLTextureRenderTarget::~GLTextureRenderTarget() {
    if (textureArray) {
        delete [] textureArray;
    }

    if (fboHandle) {
        glDeleteFramebuffers(1, &fboHandle);
    }
}

void GLTextureRenderTarget::use() {
    assert(fboHandle);
    glBindFramebuffer(GL_FRAMEBUFFER, fboHandle);
    glAssert;
}

void GLTextureRenderTarget::useTexture(int texIndex) {
    assert(texIndex < textures);
    
    currentTexture = &textureArray[texIndex];
    
    GLint origFboHandle;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &origFboHandle);
    
    glBindFramebuffer(GL_FRAMEBUFFER, fboHandle);

    glActiveTexture(GL_TEXTURE0);
    currentTexture->bind();

    glFramebufferTexture2D(GL_FRAMEBUFFER,
            GL_COLOR_ATTACHMENT0,
            GL_TEXTURE_2D,
            currentTexture->handle(), 0);

    glBindFramebuffer(GL_FRAMEBUFFER, origFboHandle);
    glAssert;
}

