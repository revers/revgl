#include <cstdio>
#include <iostream>
#include <GL/glew.h>
#include <GL/gl.h>

#include <rev/gl/RevGLAxes.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/common/RevErrorStream.h>

using namespace rev;
using namespace std;

static const char* shader = 
"//-- Vertex\n"
"#version 400\n"
"\n"
"layout(location = 0) in vec3 VertexPosition;\n"
"layout(location = 1) in vec3 VertexColor;\n"
"\n"
"uniform mat4 ModelMatrix = mat4(1.0);\n"
"uniform mat4 ViewMatrix;\n"
"uniform mat4 ProjectionMatrix;\n"
"\n"
"out vec3 Color;\n"
"\n"
"void main() {\n"
"\n"
"    vec4 position = ModelMatrix * vec4(VertexPosition, 1);\n"
"    mat4 viewProjection = ProjectionMatrix * ViewMatrix;\n"
"\n"
"    gl_Position = viewProjection * position;\n"
"\n"
"    Color = VertexColor;\n"
"}\n"
"\n"
"//-- Fragment\n"
"#version 400\n"
"\n"
"in vec3 Color;\n"
"layout(location = 0) out vec4 FragColor;\n"
"\n"
"void main() {\n"
"    FragColor = vec4(Color, 1.0);\n"
"}";

GLAxes::GLAxes(float length_ /* = 20.0f */) {
    length = length_;
}

bool GLAxes::init() {
    if (!axesProg.compileShaderGLSLString(shader)) {
        REV_ERROR_MSG("VBOAxes::init() FAILED!!!");
        return false;
    }

    glBindAttribLocation(axesProg.getHandle(), 0, "VertexPosition");
    glBindAttribLocation(axesProg.getHandle(), 1, "VertexColor");

    glBindAttribLocation(axesProg.getHandle(), 0, "FragColor");

    if (!axesProg.link()) {
        REV_ERROR_MSG("VBOAxes::init() FAILED!!!");
        return false;
    }

    // First simple object
    float* vert = new float[18]; // vertex array
    float* col = new float[18]; // color array

    //float len = 20.0f;
    float centerX = 0.0f;
    float centerY = 0.0f;
    float centerZ = 0.0f;

    col[0] = 1;
    col[1] = 0;
    col[2] = 0;

    col[3] = 1;
    col[4] = 0;
    col[5] = 0;

    col[6] = 0;
    col[7] = 1;
    col[8] = 0;

    col[9] = 0;
    col[10] = 1;
    col[11] = 0;

    col[12] = 0;
    col[13] = 0;
    col[14] = 1;

    col[15] = 0;
    col[16] = 0;
    col[17] = 1;

    vert[0] = centerX;
    vert[1] = centerY;
    vert[2] = centerZ;

    vert[3] = centerX + length;
    vert[4] = centerY;
    vert[5] = centerZ;

    vert[6] = centerX;
    vert[7] = centerY;
    vert[8] = centerZ;

    vert[9] = centerX;
    vert[10] = centerY + length;
    vert[11] = centerZ;

    vert[12] = centerX;
    vert[13] = centerY;
    vert[14] = centerZ;

    vert[15] = centerX;
    vert[16] = centerY;
    vert[17] = centerZ + length;


    // Two VAOs allocation
    glGenVertexArrays(1, &vaoHandle);

    // First VAO setup
    glBindVertexArray(vaoHandle);

    unsigned int handle[2];
    glGenBuffers(2, handle);

    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, 18 * sizeof (GLfloat), vert, GL_STATIC_DRAW);
    glVertexAttribPointer((GLuint) 0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, 18 * sizeof (GLfloat), col, GL_STATIC_DRAW);
    glVertexAttribPointer((GLuint) 1, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);

    delete [] vert;
    delete [] col;

    return true;
}

void GLAxes::render(const glm::mat4& viewMatrix,
        const glm::mat4& projectionMatrix) {

    axesProg.use();

    axesProg.setUniform("ViewMatrix", viewMatrix);
    axesProg.setUniform("ProjectionMatrix", projectionMatrix);

    glBindVertexArray(vaoHandle);
    glDrawArrays(GL_LINES, 0, 6);
}
