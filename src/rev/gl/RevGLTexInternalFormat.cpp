/* 
 * File:   RevGLTexInternalFormat.cpp
 * Author: Revers
 * 
 * Created on 10 lipiec 2012, 07:51
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include <rev/gl/RevGLTexInternalFormat.h>

using namespace rev;

void GLTexInternalFormat::calcChannelsAndComponentSize() {
    switch (format) {
        case GL_R8:
            componentSize = sizeof (GLubyte);
            channels = 1;
            break;
        case GL_R16:
            componentSize = sizeof (GLushort);
            channels = 1;
            break;
        case GL_R16F:
            componentSize = sizeof (GLhalf);
            channels = 1;
            break;
        case GL_R32F:
            componentSize = sizeof (GLfloat);
            channels = 1;
            break;
        case GL_R8I:
            componentSize = sizeof (GLbyte);
            channels = 1;
            break;
        case GL_R16I:
            componentSize = sizeof (GLshort);
            channels = 1;
            break;
        case GL_R32I:
            componentSize = sizeof (GLint);
            channels = 1;
            break;
        case GL_R8UI:
            componentSize = sizeof (GLubyte);
            channels = 1;
            break;
        case GL_R16UI:
            componentSize = sizeof (GLushort);
            channels = 1;
            break;
        case GL_R32UI:
            componentSize = sizeof (GLuint);
            channels = 1;
            break;
        case GL_RG8:
            componentSize = sizeof (GLubyte);
            channels = 2;
            break;
        case GL_RG16:
            componentSize = sizeof (GLushort);
            channels = 2;
            break;
        case GL_RG16F:
            componentSize = sizeof (GLhalf);
            channels = 2;
            break;
        case GL_RG32F:
            componentSize = sizeof (GLfloat);
            channels = 2;
            break;
        case GL_RG8I:
            componentSize = sizeof (GLbyte);
            channels = 2;
            break;
        case GL_RG16I:
            componentSize = sizeof (GLshort);
            channels = 2;
            break;
        case GL_RG32I:
            componentSize = sizeof (GLint);
            channels = 2;
            break;
        case GL_RG8UI:
            componentSize = sizeof (GLubyte);
            channels = 2;
            break;
        case GL_RG16UI:
            componentSize = sizeof (GLushort);
            channels = 2;
            break;
        case GL_RG32UI:
            componentSize = sizeof (GLuint);
            channels = 2;
            break;
        case GL_RGB32F:
            componentSize = sizeof (GLfloat);
            channels = 3;
            break;
        case GL_RGB32I:
            componentSize = sizeof (GLint);
            channels = 3;
            break;
        case GL_RGB32UI:
            componentSize = sizeof (GLuint);
            channels = 3;
            break;
        case GL_RGBA8:
            componentSize = sizeof (GLuint);
            channels = 4;
            break;
        case GL_RGBA16:
            componentSize = sizeof (GLshort);
            channels = 4;
            break;
        case GL_RGBA16F:
            componentSize = sizeof (GLhalf);
            channels = 4;
            break;
        case GL_RGBA32F:
            componentSize = sizeof (GLfloat);
            channels = 4;
            break;
        case GL_RGBA8I:
            componentSize = sizeof (GLbyte);
            channels = 4;
            break;
        case GL_RGBA16I:
            componentSize = sizeof (GLshort);
            channels = 4;
            break;
        case GL_RGBA32I:
            componentSize = sizeof (GLint);
            channels = 4;
            break;
        case GL_RGBA8UI:
            componentSize = sizeof (GLubyte);
            channels = 4;
            break;
        case GL_RGBA16UI:
            componentSize = sizeof (GLushort);
            channels = 4;
            break;
        case GL_RGBA32UI:
            componentSize = sizeof (GLuint);
            channels = 4;
            break;
        default:
            componentSize = 0;
            channels = 0;
            break;
    }
}


