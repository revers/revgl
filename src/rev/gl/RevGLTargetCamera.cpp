/* 
 * File:   GLTargetCamera.cpp
 * Author: Revers
 * 
 * Created on 22 luty 2012, 21:51
 */

#include <rev/gl/RevGLTargetCamera.h>
#include <GL/glew.h>
#include <GL/gl.h>

#include <iostream>

#define FUNC_SQRT sqrtf

//utility macros
//assuming IEEE-754(GLfloat), which i believe has max precision of 7 bits
#define EPSILON 1.0e-5

using namespace rev;

GLTargetCamera::GLTargetCamera() :
        UP(0.0f, 1.0f, 0.0f, 1.0f),
                FORWARD(0.0f, 0.0f, 1.0f, 1.0f),
                FORWARD_3D(0.0f, 0.0f, 1.0f),
                SIDE(1.0f, 0.0f, 0.0f, 1.0f),
                rotMatrix(1.0f),
                lastRotMatrix(0.0f),
                position(10),
                target(0),
                viewMatrix(1.0f) {

    forward = position - target;
    cameraDistance = glm::length(forward);
    forward /= cameraDistance;

    calcMatricesFromPosAndTarget();
}

GLTargetCamera::GLTargetCamera(const glm::vec3& position_,
        const glm::vec3& target_,
        float windowWidth,
        float windowHeight) :
        UP(0.0f, 1.0f, 0.0f, 1.0f),
                FORWARD(0.0f, 0.0f, 1.0f, 1.0f),
                FORWARD_3D(0.0f, 0.0f, 1.0f),
                SIDE(1.0f, 0.0f, 0.0f, 1.0f),
                rotMatrix(1.0f),
                lastRotMatrix(0.0f),
                position(position_),
                target(target_),
                viewMatrix(1.0f) {
    setBounds(windowWidth, windowHeight);

    forward = position - target;
    cameraDistance = glm::length(forward);
    forward /= cameraDistance;

    calcMatricesFromPosAndTarget();
}

void GLTargetCamera::click(int x, int y) {
    glm::vec2 pt((float) x, (float) y);

    lastRotMatrix = rotMatrix;
    lastPosition = glm::vec4(position, 1.0f);

    mapToSphere(&pt, &stVec);
}

void GLTargetCamera::drag(int x, int y) {

    glm::vec2 pt((float) x, (float) y);
    mapToSphere(&pt, &enVec);

    float angle = glm::degrees(acos(std::min(1.0f, glm::dot(stVec, enVec))));

    glm::vec3 axis = glm::cross(enVec, stVec);
    glm::mat4 rotation = glm::rotate(angle, axis);

    rotMatrix = lastRotMatrix * rotation;

    forward = glm::vec3(rotMatrix * FORWARD);
    side = glm::vec3(rotMatrix * SIDE);
    up = glm::vec3(rotMatrix * UP);

    position = target + forward * cameraDistance;

    viewMatrix[0][0] = side[0];
    viewMatrix[1][0] = side[1];
    viewMatrix[2][0] = side[2];
    viewMatrix[3][0] = 0;

    viewMatrix[0][1] = up[0];
    viewMatrix[1][1] = up[1];
    viewMatrix[2][1] = up[2];
    viewMatrix[3][1] = 0;

    viewMatrix[0][2] = forward[0];
    viewMatrix[1][2] = forward[1];
    viewMatrix[2][2] = forward[2];
    viewMatrix[3][2] = 0;

    viewMatrix[3][0] = 0;
    viewMatrix[3][1] = 0;
    viewMatrix[3][2] = 0;
    viewMatrix[3][3] = 1;

    viewMatrix *= glm::translate(-position);
}

void GLTargetCamera::apply() {
    glMultMatrixf(&viewMatrix[0][0]);

    float len = 2.0f;
    glm::vec3 xPoint(len, 0, 0);
    glm::vec3 yPoint(0, len, 0);
    glm::vec3 zPoint(0, 0, len);

    xPoint += target;
    yPoint += target;
    zPoint += target;

    glLineWidth(4);

    glDisable(GL_LIGHTING);
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3fv(&target[0]);
    glVertex3fv(&xPoint[0]);

    glColor3f(0, 1, 0);
    glVertex3fv(&target[0]);
    glVertex3fv(&yPoint[0]);

    glColor3f(0, 0, 1);
    glVertex3fv(&target[0]);
    glVertex3fv(&zPoint[0]);

    glEnd();

    glLineWidth(1);

}

void GLTargetCamera::calcMatricesFromPosAndTarget() {

    float angle = glm::degrees(acos(std::min(1.0f, glm::dot(FORWARD_3D, forward))));
    glm::vec3 axis(0, 1, 0);

    if (angle != 0.0f && angle != 180.0f) {
        axis = glm::cross(FORWARD_3D, forward);
    }
    rotMatrix = glm::rotate(angle, axis);

    side = glm::vec3(rotMatrix * SIDE);
    up = glm::vec3(rotMatrix * UP);

    viewMatrix[0][0] = side[0];
    viewMatrix[1][0] = side[1];
    viewMatrix[2][0] = side[2];
    viewMatrix[3][0] = 0;

    viewMatrix[0][1] = up[0];
    viewMatrix[1][1] = up[1];
    viewMatrix[2][1] = up[2];
    viewMatrix[3][1] = 0;

    viewMatrix[0][2] = forward[0];
    viewMatrix[1][2] = forward[1];
    viewMatrix[2][2] = forward[2];
    viewMatrix[3][2] = 0;

    viewMatrix[3][0] = 0;
    viewMatrix[3][1] = 0;
    viewMatrix[3][2] = 0;
    viewMatrix[3][3] = 1;

    viewMatrix *= glm::translate(-position);
}

void GLTargetCamera::mapToSphere(const glm::vec2* newPt, glm::vec3* newVec) const {
    glm::vec2 tempPt;
    GLfloat length;

    //Copy paramter into temp point
    tempPt = *newPt;

    //Adjust point coords and scale down to range of [-1 ... 1]
    tempPt.x = (tempPt.x * this->adjustWidth) - 1.0f;
    tempPt.y = 1.0f - (tempPt.y * this->adjustHeight);

    //Compute the square of the length of the vector to the point from the center
    length = (tempPt.x * tempPt.x) + (tempPt.y * tempPt.y);

    //If the point is mapped outside of the sphere... (length > radius squared)
    if (length > 1.0f) {
        GLfloat norm;

        //Compute a normalizing factor (radius / sqrt(length))
        norm = 1.0f / FUNC_SQRT(length);

        //Return the "normalized" vector, a point on the sphere
        newVec->x = tempPt.x * norm;
        newVec->y = tempPt.y * norm;
        newVec->z = 0.0f;
    } else //Else it's on the inside
    {
        //Return a vector to a point mapped inside the sphere sqrt(radius squared - length)
        newVec->x = tempPt.x;
        newVec->y = tempPt.y;
        newVec->z = FUNC_SQRT(1.0f - length);
    }
}

void GLTargetCamera::setPositionAndTarget(const glm::vec3& position,
        const glm::vec3& target) {
    this->position = position;
    this->target = target;

    forward = position - target;
    cameraDistance = glm::length(forward);
    forward /= cameraDistance;

    calcMatricesFromPosAndTarget();
}

void GLTargetCamera::setPosition(const glm::vec3& position) {
    this->position = position;

    forward = position - target;
    cameraDistance = glm::length(forward);
    forward /= cameraDistance;

    calcMatricesFromPosAndTarget();
}

void GLTargetCamera::setTarget(const glm::vec3& target) {
    this->target = target;

    forward = position - target;
    cameraDistance = glm::length(forward);
    forward /= cameraDistance;

    calcMatricesFromPosAndTarget();
}

void GLTargetCamera::setCameraDistance(float cameraDistance) {
    this->cameraDistance = cameraDistance;

    viewMatrix[3][0] = 0;
    viewMatrix[3][1] = 0;
    viewMatrix[3][2] = 0;
    viewMatrix[3][3] = 1;

    position = target + forward * cameraDistance;

    viewMatrix *= glm::translate(-position);
}
