

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <cstdio>
#include <cstdlib>

#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevCommonUtil.h>
#include <rev/common/RevErrorStream.h>

using namespace rev;

const char* rev::glGetErrorString(GLenum code) {
    switch (code) {

        case 0: return "GL_NO_ERROR";
        case 0x0500: return "GL_INVALID_ENUM";
        case 0x0501: return "GL_INVALID_VALUE";
        case 0x0502: return "GL_INVALID_OPERATION";
        case 0x0503: return "GL_STACK_OVERFLOW | GL_OUT_OF_MEMORY";
        case 0x0504: return "GL_STACK_UNDERFLOW";
        case 0x0505: return "GL_OUT_OF_MEMORY";
        case 0x0506: return "GL_INVALID_FRAMEBUFFER_OPERATION";
        case 0xFFFFFFFF: return "GL_INVALID_INDEX";

        default: return "UNKNOWN_ERROR";
    }

    return NULL;
}

void rev::glAssert_(const char* function, const char * file, int line) {

    GLenum glErr = glGetError();
    if (glErr != GL_NO_ERROR) {
        REV_ERROR_MSG("OPENGL ERROR " << rev::glGetErrorString(glErr)
                << "; " << function << "; " << gluErrorString(glErr)
                << "\n\t" << file << " @ line " << line << "\n");
        fflush(stdout);

        __asm("int $3");
    }
}

void rev::glAssertNoExit_(const char* function, const char * file, int line) {

    GLenum glErr = glGetError();
    while (glErr != GL_NO_ERROR) {

       REV_ERROR_MSG("OPENGL ERROR " << rev::glGetErrorString(glErr)
                << "; " << function << "; " << gluErrorString(glErr)
                << "\n\t" << file << " @ line " << line << "\n");
        fflush(stdout);
        glErr = glGetError();
    }
}

