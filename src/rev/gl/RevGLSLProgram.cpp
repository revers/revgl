#include <fstream>
#include <sstream>
#include <iostream>
#include <sys/stat.h>
#include <rev/gl/RevGLSLProgram.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace std;
using namespace glm;

#define VERTEX_SHADER "//-- Vertex"
#define FRAGMENT_SHADER "//-- Fragment"
#define GEOMETRY_SHADER "//-- Geometry"
#define TESS_CONTROL_SHADER "//-- TessControl"
#define TESS_EVAL_SHADER "//-- TessEval"

GLSLProgram::GLSLProgram() :
		handle(0), linked(false),
				verbose(false),
				vertexShaderHandle(0),
				fragmentShaderHandle(0),
				geometryShaderHandle(0),
				tessControlShaderHandle(0),
				tessEvaluationShaderHandle(0) {

}

GLSLProgram::~GLSLProgram() {
	if (!handle)
		return;

	glUseProgram(0);

	if (vertexShaderHandle)
		glDetachShader(handle, vertexShaderHandle);
	if (fragmentShaderHandle)
		glDetachShader(handle, fragmentShaderHandle);
	if (geometryShaderHandle)
		glDetachShader(handle, geometryShaderHandle);
	if (tessControlShaderHandle)
		glDetachShader(handle, tessControlShaderHandle);
	if (tessEvaluationShaderHandle)
		glDetachShader(handle, tessEvaluationShaderHandle);

	glDeleteProgram(handle);

	/*
	 * http://www.opengl.org/sdk/docs/man4/xhtml/glDeleteShader.xml
	 *
	 * "If a shader object to be deleted is attached to a program
	 * object, it will be flagged for deletion, but it will
	 * not be deleted  until it is no longer attached to any program
	 * object, for any rendering context (i.e., it must be detached
	 * from wherever it was attached before it will be deleted)"
	 *
	 * So we can share shaders among programs and use glDeleteShader
	 * in every of them without worry about consequences of
	 * deleting shader few times.
	 */

	if (vertexShaderHandle)
		glDeleteShader(vertexShaderHandle);
	if (fragmentShaderHandle)
		glDeleteShader(fragmentShaderHandle);
	if (geometryShaderHandle)
		glDeleteShader(geometryShaderHandle);
	if (tessControlShaderHandle)
		glDeleteShader(tessControlShaderHandle);
	if (tessEvaluationShaderHandle)
		glDeleteShader(tessEvaluationShaderHandle);
}

string trim(string& line) {
	int begin = 0;
	while (begin < line.length() &&
			(line[begin] == ' ' || line[begin] == '\t')) {
		begin++;
	}

	int len = line.length() - begin;
	int end = begin + len - 1;
	char c = line[end];

	while ((end > begin) && (c == ' ' || c == '\t')) {
		c = line[--end];
		len--;
	}

	return line.substr(begin, len);
}

bool GLSLProgram::compileShaderGLSLStream(std::istream& in) {
	string vertexShaderStr;
	string fragmentShaderStr;
	string geometryShaderStr;
	string tessControlShaderStr;
	string tessEvalShaderStr;

	std::string line;

	GLSLShader::GLSLShaderType shaderType = GLSLShader::NONE;

	while (std::getline(in, line)) {

		line = trim(line);

		if (line == VERTEX_SHADER) {
			if (verbose)
				REV_ERROR_MSG("Found Vertex Shader");
			shaderType = GLSLShader::VERTEX;
		} else if (line == FRAGMENT_SHADER) {
			if (verbose)
				REV_ERROR_MSG("Found Fragment Shader");
			shaderType = GLSLShader::FRAGMENT;
		} else if (line == GEOMETRY_SHADER) {
			if (verbose)
				REV_ERROR_MSG("Found Geometry Shader");
			shaderType = GLSLShader::GEOMETRY;
		} else if (line == TESS_CONTROL_SHADER) {
			if (verbose)
				REV_ERROR_MSG("Found Tess Control Shader");
			shaderType = GLSLShader::TESS_CONTROL;
		} else if (line == TESS_EVAL_SHADER) {
			if (verbose)
				REV_ERROR_MSG("Found Tess Eval Shader");
			shaderType = GLSLShader::TESS_EVALUATION;
		}

		switch (shaderType) {
		case GLSLShader::VERTEX:
			vertexShaderStr += line;
			vertexShaderStr += '\n';
			break;
		case GLSLShader::FRAGMENT:
			fragmentShaderStr += line;
			fragmentShaderStr += '\n';
			break;
		case GLSLShader::GEOMETRY:
			geometryShaderStr += line;
			geometryShaderStr += '\n';
			break;
		case GLSLShader::TESS_CONTROL:
			tessControlShaderStr += line;
			tessControlShaderStr += '\n';
			break;
		case GLSLShader::TESS_EVALUATION:
			tessEvalShaderStr += line;
			tessEvalShaderStr += '\n';
			break;
		default:
			break;
		}

	}

	if (vertexShaderStr.size() > 0) {
		bool succ = compileShaderFromString(vertexShaderStr, GLSLShader::VERTEX);
		if (!succ) {
			REV_ERROR_MSG("Vertex Shader compilation FAILED!!\n\t" << logString);
			return false;
		}
	}

	if (fragmentShaderStr.size() > 0) {
		bool succ = compileShaderFromString(fragmentShaderStr,
				GLSLShader::FRAGMENT);
		if (!succ) {
			REV_ERROR_MSG("Fragment Shader compilation FAILED!!\n\t" << logString);
			return false;
		}
	}

	if (geometryShaderStr.size() > 0) {
		bool succ = compileShaderFromString(geometryShaderStr,
				GLSLShader::GEOMETRY);
		if (!succ) {
			REV_ERROR_MSG("Geometry Shader compilation FAILED!!\n\t" << logString);
			return false;
		}
	}

	if (tessControlShaderStr.size() > 0) {
		bool succ = compileShaderFromString(tessControlShaderStr,
				GLSLShader::TESS_CONTROL);
		if (!succ) {
			REV_ERROR_MSG(
					"Tess Control Shader compilation FAILED!!\n\t" << logString);
			return false;
		}
	}

	if (tessEvalShaderStr.size() > 0) {
		bool succ = compileShaderFromString(tessEvalShaderStr,
				GLSLShader::TESS_EVALUATION);
		if (!succ) {
			REV_ERROR_MSG("Tess Eval Shader compilation FAILED!!\n\t" << logString);
			return false;
		}
	}

	if (verbose)
		REV_ERROR_MSG("Shader compilation SUCCEEDED.");

	return true;
}

bool GLSLProgram::compileShaderGLSLString(const char* shaderStr) {
	std::istringstream iss(shaderStr);
	return compileShaderGLSLStream(iss);
}

bool GLSLProgram::compileShaderGLSLFile(const char* filename) {

	if (verbose)
		REV_ERROR_MSG("Reading File: '" << filename << "'...");

	std::ifstream in(filename);

	if (!in) {
		logString = "File not found.";
		string msg("ERROR: FILE NOT FOUND: '");
		msg += filename;
		msg += "'!!";
		REV_ERROR_MSG(msg);

		return false;
	}

	return compileShaderGLSLStream(in);
}

bool GLSLProgram::compileShaderFromFile(const char* fileName,
		GLSLShader::GLSLShaderType type) {

	if (!createProgramIfNeeded()) {
		return false;
	}

	GLuint shaderHandle = staticCompileShaderFromFile(fileName, type);
	if (shaderHandle == 0) {
		return false;
	}

	switch (type) {
	case GLSLShader::VERTEX:
		vertexShaderHandle = shaderHandle;
		break;
	case GLSLShader::FRAGMENT:
		fragmentShaderHandle = shaderHandle;
		break;
	case GLSLShader::GEOMETRY:
		geometryShaderHandle = shaderHandle;
		break;
	case GLSLShader::TESS_CONTROL:
		tessControlShaderHandle = shaderHandle;
		break;
	case GLSLShader::TESS_EVALUATION:
		tessEvaluationShaderHandle = shaderHandle;
		break;
	}

	glAttachShader(handle, shaderHandle);

	return true;
}

bool GLSLProgram::compileShaderFromString(const string& source,
		GLSLShader::GLSLShaderType type) {

	if (!createProgramIfNeeded()) {
		return false;
	}

	GLuint shaderHandle = staticCompileShaderFromString(source, type);
	if (shaderHandle == 0) {
		return false;
	}

	switch (type) {
	case GLSLShader::VERTEX:
		vertexShaderHandle = shaderHandle;
		break;
	case GLSLShader::FRAGMENT:
		fragmentShaderHandle = shaderHandle;
		break;
	case GLSLShader::GEOMETRY:
		geometryShaderHandle = shaderHandle;
		break;
	case GLSLShader::TESS_CONTROL:
		tessControlShaderHandle = shaderHandle;
		break;
	case GLSLShader::TESS_EVALUATION:
		tessEvaluationShaderHandle = shaderHandle;
		break;
	}

	glAttachShader(handle, shaderHandle);

	return true;
}

GLuint GLSLProgram::staticCompileShaderFromFile(
		const char* fileName,
		GLSLShader::GLSLShaderType type) {

	std::ifstream in(fileName);

	if (!in) {
		string msg("ERROR: FILE NOT FOUND: '");
		msg += fileName;
		msg += "'!!";
		REV_ERROR_MSG(msg);

		return 0;
	}

	std::string line;
	std::string prefix = "#include";

	string shader;

	while (std::getline(in, line)) {
		if (line.size() >= prefix.size() &&
				line.substr(0, prefix.size()) == prefix) {
			continue;
		}

		shader += line;
		shader += "\n";
	}
	in.close();

	return staticCompileShaderFromString(shader, type);
}

GLuint GLSLProgram::staticCompileShaderFromString(
		const string& source,
		GLSLShader::GLSLShaderType type) {

	GLuint shaderHandle = 0;

	switch (type) {
	case GLSLShader::VERTEX:
		shaderHandle = glCreateShader(GL_VERTEX_SHADER);
		break;
	case GLSLShader::FRAGMENT:
		shaderHandle = glCreateShader(GL_FRAGMENT_SHADER);
		break;
	case GLSLShader::GEOMETRY:
		shaderHandle = glCreateShader(GL_GEOMETRY_SHADER);
		break;
	case GLSLShader::TESS_CONTROL:
		shaderHandle = glCreateShader(GL_TESS_CONTROL_SHADER);
		break;
	case GLSLShader::TESS_EVALUATION:
		shaderHandle = glCreateShader(GL_TESS_EVALUATION_SHADER);
		break;
	default:
		return false;
	}

	const char* c_code = source.c_str();
	glShaderSource(shaderHandle, 1, &c_code, NULL);

	// Compile the shader
	glCompileShader(shaderHandle);

	// Check for errors
	int result;
	glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &result);
	if (GL_FALSE == result) {
		int length = 0;
		glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &length);
		if (length > 0) {
			char* c_log = new char[length];
			int written = 0;
			glGetShaderInfoLog(shaderHandle, length, &written, c_log);
			REV_ERROR_MSG("ERROR: " << c_log);
			delete[] c_log;
		}

		return 0;
	}

	return shaderHandle;
}

bool GLSLProgram::link() {
	if (linked) {
		return true;
	}
	if (handle == 0) {
		logString = "Program isn't created. Probably there "
				"are no shaders attached to this program!!";
		REV_ERROR_MSG("ERROR: " << logString);

		return false;
	}

	glLinkProgram(handle);

	int status = 0;
	glGetProgramiv(handle, GL_LINK_STATUS, &status);
	if (GL_FALSE == status) {
		// Store log and return false
		int length = 0;
		logString = "";

		glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &length);

		if (length > 0) {
			char* c_log = new char[length];
			int written = 0;
			glGetProgramInfoLog(handle, length, &written, c_log);
			logString = c_log;
			delete[] c_log;
		}

		REV_ERROR_MSG("LINKING FAILED!!\n\t" << logString);

		return false;
	} else {
		linked = true;
		if (verbose)
			REV_ERROR_MSG("Shader linking SUCCEEDED.");
		return linked;
	}
}

void GLSLProgram::use() {
	assert(handle);
	assert(linked);
	//if (handle == 0 || (!linked)) return;
	glUseProgram(handle);
	glAssert;
}

void GLSLProgram::unuse() {
	glUseProgram(0);
}

string GLSLProgram::log() {
	return logString;
}

GLuint GLSLProgram::getHandle() {
	return handle;
}

bool GLSLProgram::isLinked() {
	return linked;
}

void GLSLProgram::bindAttribLocation(GLuint location, const char* name) {
	glBindAttribLocation(handle, location, name);
	glAssert;
}

void GLSLProgram::bindFragDataLocation(GLuint location, const char* name) {
	glBindFragDataLocation(handle, location, name);
	glAssert;
}

void GLSLProgram::setUniform(const char* name, float x, float y, float z) {
	int loc = getUniformLocation(name);
	glAssert;
	if (loc >= 0) {
		glUniform3f(loc, x, y, z);
		glAssert;
	}
}

void GLSLProgram::setUniform(const char* name, float x, float y) {
	int loc = getUniformLocation(name);
	glAssert;
	if (loc >= 0) {
		glUniform2f(loc, x, y);
		glAssert;
	}
}

void GLSLProgram::setUniform(const char* name, const vec2& v) {
	this->setUniform(name, v.x, v.y);
}

void GLSLProgram::setUniform(const char* name, const vec3& v) {
	this->setUniform(name, v.x, v.y, v.z);
}

void GLSLProgram::setUniform(const char* name, const vec4& v) {
	int loc = getUniformLocation(name);
	glAssert;
	if (loc >= 0) {
		glUniform4f(loc, v.x, v.y, v.z, v.w);
		glAssert;
	}
}

void GLSLProgram::setUniform(const char* name, const rev::color3& c) {
	this->setUniform(name, c.r, c.g, c.b);
}

void GLSLProgram::setUniform(const char* name, const color4& c) {
	int loc = getUniformLocation(name);
	glAssert;
	if (loc >= 0) {
		glUniform4f(loc, c.r, c.g, c.b, c.a);
		glAssert;
	}
}

void GLSLProgram::setUniform(const char* name, const mat4& m) {
	int loc = getUniformLocation(name);
	glAssert;
	if (loc >= 0) {
		glUniformMatrix4fv(loc, 1, GL_FALSE, &m[0][0]);
		glAssert;
	}
}

void GLSLProgram::setUniform(const char* name, const mat3& m) {
	int loc = getUniformLocation(name);
	glAssert;
	if (loc >= 0) {
		glUniformMatrix3fv(loc, 1, GL_FALSE, &m[0][0]);
		glAssert;
	}
}

void GLSLProgram::setUniform(const char* name, float val) {
	int loc = getUniformLocation(name);
	glAssert;
	if (loc >= 0) {
		glUniform1f(loc, val);
		glAssert;
	}
}

void GLSLProgram::setUniform(const char* name, int val) {
	int loc = getUniformLocation(name);
	glAssert;
	if (loc >= 0) {
		glUniform1i(loc, val);
		glAssert;
	}
}

void GLSLProgram::setUniform(const char* name, unsigned int val) {
	int loc = getUniformLocation(name);
	glAssert;
	if (loc >= 0) {
		glUniform1ui(loc, val);
		glAssert;
	}
}

void GLSLProgram::setUniform(const char* name, bool val) {
	int loc = getUniformLocation(name);
	glAssert;
	if (loc >= 0) {
		glUniform1i(loc, val);
		glAssert;
	}
}

void GLSLProgram::setUniform(int location, float x, float y) {
	glUniform2f(location, x, y);
	glAssert;
}

void GLSLProgram::setUniform(int location, const vec4& v) {
	glUniform4f(location, v.x, v.y, v.z, v.w);
	glAssert;
}

void GLSLProgram::setUniform(int location, const rev::color4& c) {
	glUniform4f(location, c.r, c.g, c.b, c.a);
	glAssert;
}

void GLSLProgram::setUniform(int location, const mat4& m) {
	glUniformMatrix4fv(location, 1, GL_FALSE, &m[0][0]);
	glAssert;
}

void GLSLProgram::setUniform(int location, const mat3& m) {
	glUniformMatrix3fv(location, 1, GL_FALSE, &m[0][0]);
	glAssert;
}

void GLSLProgram::setUniform(int location, float val) {
	glUniform1f(location, val);
	glAssert;
}

void GLSLProgram::setUniform(int location, int val) {
	glUniform1i(location, val);
	glAssert;
}

void GLSLProgram::setUniform(int location, unsigned int val) {
	glUniform1ui(location, val);
	glAssert;
}

void GLSLProgram::setUniform(int location, bool val) {
	glUniform1i(location, val);
	glAssert;
}

void GLSLProgram::setUniform(int location, float x, float y, float z) {
	glUniform3f(location, x, y, z);
	glAssert;
}

void GLSLProgram::setSubroutine(const char* name, GLenum shaderType) {
	GLuint index = getSubroutineIndex(name, shaderType);
	glAssert;
	if (index != GL_INVALID_INDEX) {
		glUniformSubroutinesuiv(shaderType, 1, &index);
		glAssert;
	} else {
		REV_ERROR_MSG("Subroutine: " << name << " not found.");
	}
}

void GLSLProgram::printActiveUniforms() {
	GLint nUniforms, size, location, maxLen;
	GLchar* name;
	GLsizei written;
	GLenum type;

	glGetProgramiv(handle, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxLen);
	glGetProgramiv(handle, GL_ACTIVE_UNIFORMS, &nUniforms);

	name = (GLchar*) malloc(maxLen);

	printf(" Location | Name\n");
	printf("------------------------------------------------\n");
	for (int i = 0; i < nUniforms; ++i) {
		glGetActiveUniform(handle, i, maxLen, &written, &size, &type, name);
		location = glGetUniformLocation(handle, name);
		printf(" %-8d | %s\n", location, name);
	}

	free(name);
}

void GLSLProgram::printActiveAttribs() {

	GLint written, size, location, maxLength, nAttribs;
	GLenum type;
	GLchar* name;

	glGetProgramiv(handle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxLength);
	glGetProgramiv(handle, GL_ACTIVE_ATTRIBUTES, &nAttribs);

	name = (GLchar*) malloc(maxLength);

	printf(" Index | Name\n");
	printf("------------------------------------------------\n");
	for (int i = 0; i < nAttribs; i++) {
		glGetActiveAttrib(handle, i, maxLength, &written, &size, &type, name);
		location = glGetAttribLocation(handle, name);
		printf(" %-5d | %s\n", location, name);
	}

	free(name);
}

bool GLSLProgram::validate() {
	if (!isLinked())
		return false;

	GLint status;
	glValidateProgram(handle);
	glGetProgramiv(handle, GL_VALIDATE_STATUS, &status);

	if (GL_FALSE == status) {
		// Store log and return false
		int length = 0;
		logString = "";

		glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &length);

		if (length > 0) {
			char* c_log = new char[length];
			int written = 0;
			glGetProgramInfoLog(handle, length, &written, c_log);
			logString = c_log;
			delete[] c_log;
		}

		return false;
	} else {
		return true;
	}
}

int GLSLProgram::getUniformLocation(const char* name) {
	int loc = glGetUniformLocation(handle, name);
	alwaysAssertMsgNoExit(loc >= 0, "Uniform '" << name << "' not found!");
	return loc;
}

bool GLSLProgram::fileExists(const string& fileName) {
	struct stat info;
	int ret = -1;

	ret = stat(fileName.c_str(), &info);
	return 0 == ret;
}

/**
 * WARNING: Do not set shader if there is already
 * compiled that type of shader. This could lead to
 * a memory leaks.
 *
 * @return true if succeed, false otherwise.
 */
bool GLSLProgram::setVertexShader(GLuint vertexShaderHandle) {
	this->vertexShaderHandle = vertexShaderHandle;
	if (!createProgramIfNeeded()) {
		return false;
	}

	glAttachShader(handle, vertexShaderHandle);
	glAssert;

	return true;
}

/**
 * WARNING: Do not set shader if there is already
 * compiled that type of shader. This could lead to
 * a memory leaks.
 *
 * @return true if succeed, false otherwise.
 */
bool GLSLProgram::setFragmentShader(GLuint fragmentShader) {
	this->fragmentShaderHandle = fragmentShader;

	if (!createProgramIfNeeded()) {
		return false;
	}

	glAttachShader(handle, fragmentShader);
	glAssert;

	return true;
}

/**
 * WARNING: Do not set shader if there is already
 * compiled that type of shader. This could lead to
 * a memory leaks.
 *
 * @return true if succeed, false otherwise.
 */
bool GLSLProgram::setGeometryShader(GLuint geometryShader) {
	this->geometryShaderHandle = geometryShader;

	if (!createProgramIfNeeded()) {
		return false;
	}

	glAttachShader(handle, geometryShader);
	glAssert;

	return true;
}

/**
 * WARNING: Do not set shader if there is already
 * compiled that type of shader. This could lead to
 * a memory leaks.
 *
 * @return true if succeed, false otherwise.
 */
bool GLSLProgram::setTessControlShader(GLuint tessControlShader) {
	this->tessControlShaderHandle = tessControlShader;

	if (!createProgramIfNeeded()) {
		return false;
	}

	glAttachShader(handle, tessControlShader);
	glAssert;

	return true;
}

/**
 * WARNING: Do not set shader if there is already
 * compiled that type of shader. This could lead to
 * a memory leaks.
 *
 * @return true if succeed, false otherwise.
 */
bool GLSLProgram::setTessEvaluationShader(GLuint tessEvaluationShader) {
	this->tessEvaluationShaderHandle = tessEvaluationShader;

	if (!createProgramIfNeeded()) {
		return false;
	}

	glAttachShader(handle, tessEvaluationShader);
	glAssert;

	return true;
}

bool GLSLProgram::createProgramIfNeeded() {
	if (handle) {
		return true;
	}

	handle = glCreateProgram();
	if (handle == 0) {
		logString = "Unable to create shader program.";
		REV_ERROR_MSG(logString);
		return false;
	}

	return true;
}
