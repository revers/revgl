#include <rev/gl/RevGLUtil.h>

using namespace rev;

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <cstdio>
#include <sstream>
#include <iosfwd>

using namespace std;

GLUtil::GLUtil() {
}

int GLUtil::checkForOpenGLError(const char * file, int line) {
    //
    // Returns 1 if an OpenGL error occurred, 0 otherwise.
    //
    GLenum glErr;
    int retCode = 0;

    glErr = glGetError();
    while (glErr != GL_NO_ERROR) {
        printf("glError in file %s @ line %d: %s\n", file, line, gluErrorString(glErr));
        retCode = 1;
        glErr = glGetError();
    }
    return retCode;

}

void GLUtil::dumpGLInfo(bool dumpExtensions) {
    const GLubyte *renderer = glGetString(GL_RENDERER);
    const GLubyte *vendor = glGetString(GL_VENDOR);
    const GLubyte *version = glGetString(GL_VERSION);
    const GLubyte *glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);

    GLint major, minor;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);

    printf("GL Vendor    : %s\n", vendor);
    printf("GL Renderer  : %s\n", renderer);
    printf("GL Version   : %s\n", version);
    printf("GL Version   : %d.%d\n", major, minor);
    printf("GLSL Version : %s\n", glslVersion);

    if (dumpExtensions) {
        GLint nExtensions;
        glGetIntegerv(GL_NUM_EXTENSIONS, &nExtensions);
        for (int i = 0; i < nExtensions; i++) {
            printf("%s\n", glGetStringi(GL_EXTENSIONS, i));
        }

        fflush(stdout);
    }
}

std::string GLUtil::getGLInfo(bool dumpExtensions /* = false */) {
    const GLubyte *renderer = glGetString(GL_RENDERER);
    const GLubyte *vendor = glGetString(GL_VENDOR);
    const GLubyte *version = glGetString(GL_VERSION);
    const GLubyte *glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);

    GLint major, minor;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);
    
    ostringstream oss;
    oss << "GL Vendor    : " << vendor << endl;
    oss << "GL Renderer  : " << renderer << endl;
    oss << "GL Version   : " << version << endl;
    oss << "GL Version   : " << major << "." << minor << endl;
    oss << "GLSL Version : " << glslVersion << endl;

    if (dumpExtensions) {
        GLint nExtensions;
        glGetIntegerv(GL_NUM_EXTENSIONS, &nExtensions);
        for (int i = 0; i < nExtensions; i++) {
            oss << glGetStringi(GL_EXTENSIONS, i) << endl;
        }

        fflush(stdout);
    }
    
    return oss.str();
}

int GLUtil::getSize(int glSizeType) {
    switch (glSizeType) {
        case GL_BYTE: return sizeof (GLbyte);
        case GL_UNSIGNED_BYTE: return sizeof (GLubyte);
        case GL_SHORT: return sizeof (GLshort);
        case GL_UNSIGNED_SHORT: return sizeof (GLushort);
        case GL_INT: return sizeof (GLint);
        case GL_UNSIGNED_INT: return sizeof (GLuint);
        case GL_FLOAT: return sizeof (GLfloat);
        case GL_DOUBLE: return sizeof (GLdouble);
        case GL_2_BYTES: return 2;
        case GL_3_BYTES: return 3;
        case GL_4_BYTES: return 4;
        case GL_RGB: return 3;
        case GL_RGBA: return 4;
        default: return 1;
    }
}

