#include <iostream>
#include <fstream>

#include <GL/glew.h>
#include <SOIL.h>
#include <GL/gl.h>

#include <rev/gl/RevGLTexture2D.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>

using namespace rev;
using namespace std;

bool GLTexture2D::load(const char* filename) {
    fstream in(filename);
    if (!in) {
        REV_ERROR_MSG("File '" << filename << "' not found!");
        return false;
    }

    int origChannels;

    int w;
    int h;
    unsigned char* data = SOIL_load_image(
            filename,
            &w, &h,
            &origChannels,
            SOIL_LOAD_RGBA);

    if (!data) {
        return false;
    }

    destroy();
    glAssert;

    for (int j = 0; j * 2 < h; ++j) {
        int index1 = j * w * 4;
        int index2 = (h - 1 - j) * w * 4;
        for (int i = w * 4; i > 0; --i) {
            unsigned char temp = data[index1];
            data[index1] = data[index2];
            data[index2] = temp;
            ++index1;
            ++index2;
        }
    }

    GLuint texture = 0;
    glGenTextures(1, &texture);
    glAssert;

    if (!texture) {
        return false;
    }

    this->tex = texture;
    setInitialSize((int) w, (int) h);

    glBindTexture(textureType, tex);
    glAssert;

    glTexImage2D(textureType, 0, internalFormat.getFormat(), w, h, 0,
            GL_RGBA, GL_UNSIGNED_BYTE, data);
    glAssert;

    glBindTexture(textureType, 0);
    updateSampler();

    SOIL_free_image_data(data);

    sizeInBytes = width * height * internalFormat.getSize();

    return true;
}

bool GLTexture2D::create(int width, int height) {
    destroy();

    setInitialSize(width, height);

    GLuint texture = 0;
    glGenTextures(1, &texture);
    glAssert;

    if (!texture) {
        return false;
    }

    this->tex = texture;

    glBindTexture(textureType, tex);
    glAssert;

    glTexImage2D(textureType, 0, internalFormat.getFormat(), width, height,
            0, uploadFormat.channelFormat, uploadFormat.channelType, 0);
    glAssert;
    //updateSampler();

    glTexParameteri(textureType, GL_TEXTURE_MIN_FILTER, sampler.minFilter);
    glTexParameteri(textureType, GL_TEXTURE_MAG_FILTER, sampler.magFilter);

    glTexParameteri(textureType, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(textureType, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glAssert;

    sizeInBytes = width * height * internalFormat.getSize();

    return true;
}

void GLTexture2D::updateSampler() {
    assert(tex);
    glBindTexture(textureType, tex);
    glAssert;
    glTexParameteri(textureType, GL_TEXTURE_MIN_FILTER, sampler.minFilter);
    glTexParameteri(textureType, GL_TEXTURE_MAG_FILTER, sampler.magFilter);

    glTexParameteri(textureType, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(textureType, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glAssert;
    glBindTexture(textureType, 0);
}

void GLTexture2D::setSampler(GLSampler2D sampler) {
    this->sampler = sampler;

    updateSampler();
}

void GLTexture2D::setSampler(GLenum minFilter, GLenum magFilter,
        GLenum wrapS, GLenum wrapT) {
    sampler.set(minFilter, magFilter, wrapS, wrapT);

    updateSampler();
}

void GLTexture2D::setFilter(GLenum minFilter, GLenum magFilter) {
    sampler.setFilter(minFilter, magFilter);

    assert(tex);
    glBindTexture(textureType, tex);

    glTexParameteri(textureType, GL_TEXTURE_MIN_FILTER, sampler.minFilter);
    glTexParameteri(textureType, GL_TEXTURE_MAG_FILTER, sampler.magFilter);
    glAssert;
    glBindTexture(textureType, 0);

}

void GLTexture2D::setWrap(GLenum wrapS, GLenum wrapT) {
    sampler.setWrap(wrapS, wrapT);

    assert(tex);
    glBindTexture(textureType, tex);

    glTexParameteri(textureType, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(textureType, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glAssert;
    glBindTexture(textureType, 0);
}

void GLTexture2D::bindToUnit(GLenum textureUnit) {
    assert(tex);
    glActiveTexture(textureUnit);

    glBindTexture(textureType, tex);
    glAssert;

    glActiveTexture(GL_TEXTURE0);
}

void GLTexture2D::unbindFromUnit(GLenum textureUnit) {
    glActiveTexture(textureUnit);

    glBindTexture(textureType, 0);

    glActiveTexture(GL_TEXTURE0);
}

void GLTexture2D::bind() {
    assert(tex);
    glBindTexture(textureType, tex);
    glAssert;
}