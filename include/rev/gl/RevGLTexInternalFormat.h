/* 
 * File:   RevGLTexInternalFormat.hpp
 * Author: Revers
 *
 * Created on 10 lipiec 2012, 06:42
 */

#ifndef REVGLTEXINTERNALFORMAT_HPP
#define	REVGLTEXINTERNALFORMAT_HPP

#include "RevGLConfig.h"

#include <GL/gl.h>

namespace rev {

    /**
     * rev::GLTexInternalFormat
     * 
     * @see <a href="http://www.opengl.org/sdk/docs/man4/xhtml/glTexImage2D.xml">glTexImage2D</a>
     */
    struct REVGL_API GLTexInternalFormat {
    private:
        int channels;
        int componentSize;
        GLenum format;

    public:

        /**
         * Most common formats are:
         *      GL_RGB8
         *      GL_RGBA8
         *      GL_RGB16F
         *      GL_RGBA16F
         *      GL_RGB32F
         *      GL_RGBA32F
         * but there are many more (described in the OpenGL API reference).
         * 
         */
        GLTexInternalFormat(GLenum format_) : format(format_) {
            calcChannelsAndComponentSize();
        }

        int getSize() const {
            return channels * componentSize;
        }

        int getChannels() const {
            return channels;
        }

        int getComponentSize() const {
            return componentSize;
        }

        GLenum getFormat() const {
            return format;
        }
        
        void set(GLenum format) {
            this->format = format;
            calcChannelsAndComponentSize();
        }

    private:

        void calcChannelsAndComponentSize();

    };

}

#endif	/* REVGLTEXINTERNALFORMAT_HPP */

