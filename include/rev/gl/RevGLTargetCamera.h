/* 
 * File:   TargetCamera.h
 * Author: Revers
 *
 * Created on 22 luty 2012, 21:51
 */

#ifndef REVGLTARGETCAMERA_H
#define	REVGLTARGETCAMERA_H

#include "RevGLConfig.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include "RevICamera.h"

namespace rev {

    class REVGL_API GLTargetCamera: public ICamera {
    private:
        const glm::vec4 UP;
        const glm::vec4 FORWARD;
        const glm::vec4 SIDE;
        const glm::vec3 FORWARD_3D;

        glm::mat4 rotMatrix;
        glm::mat4 lastRotMatrix;

        glm::vec3 stVec;
        glm::vec3 enVec;

        glm::vec3 position;
        glm::vec4 lastPosition;
        glm::vec3 target;
        glm::vec3 forward;
        glm::vec3 side;
        glm::vec3 up;
        float cameraDistance;

        float adjustWidth;
        float adjustHeight;

        glm::mat4 viewMatrix;

    public:
        GLTargetCamera();

        GLTargetCamera(const glm::vec3& position_,
                const glm::vec3& target_,
                float windowWidth,
                float windowHeight);

        void setPositionAndTarget(const glm::vec3& position,
                const glm::vec3& target);
        void setPosition(const glm::vec3& position);
        void setTarget(const glm::vec3& target);
        void setCameraDistance(float cameraDistance);

        glm::vec3 getTarget() const {
            return target;
        }

        float getCameraDistance() const {
            return cameraDistance;
        }

        const glm::vec3& getPosition() const override {
            return position;
        }

        const glm::vec3& getDirection() const override {
            return forward;
        }

        const glm::mat4& getViewMatrix() const override {
            return viewMatrix;
        }

        void increaseDistance(float val) {
            float temp = cameraDistance + val;
            setCameraDistance(temp > 0 ? temp : 4.0f);
        }

        void setBounds(float newWidth, float newHeight) {
            assert((newWidth > 1.0f) && (newHeight > 1.0f));

            //Set adjustment factor for width/height
            this->adjustWidth = 1.0f / ((newWidth - 1.0f) * 0.5f);
            this->adjustHeight = 1.0f / ((newHeight - 1.0f) * 0.5f);
        }

        void click(int x, int y);
        void drag(int x, int y);
        void apply();

    private:

        void calcMatricesFromPosAndTarget();

        void mapToSphere(const glm::vec2* newPt, glm::vec3* newVec) const;

    };
}
#endif	/* REVGLTARGETCAMERA_H */

