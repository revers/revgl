/* 
 * File:   RevTextureRenderTarget.h
 * Author: Revers
 *
 * Created on 3 czerwiec 2012, 13:01
 */

#ifndef REVGLTEXTURERENDERTARGET_H
#define	REVGLTEXTURERENDERTARGET_H

#include <vector>

#include "RevGLConfig.h"

#include "RevGLTexture2D.h"
#include "RevGLSampler2D.hpp"
#include "RevGLTexInternalFormat.h"

#include <rev/common/RevAssert.h>

#include <GL/glew.h>
#include <GL/gl.h>

namespace rev {

    class GLTextureRenderTarget;

    class REVGL_API GLTextureRenderTarget : public IBuffer<GLTextureRenderTarget> {
        GLTexture2D* currentTexture;
        GLuint fboHandle;

        int width;
        int height;
        GLTexInternalFormat format;
        GLSampler2D sampler;

        int textures;
        int currentTexIndex;
        GLTexture2D* textureArray;

        GLTextureRenderTarget(const GLTextureRenderTarget&);
        GLTextureRenderTarget& operator=(const GLTextureRenderTarget&);

    public:

        /**
         * Most common formats are:
         *      GL_RGB8
         *      GL_RGBA8
         *      GL_RGB16F
         *      GL_RGBA16F
         *      GL_RGB32F
         *      GL_RGBA32F
         * but there are many more (described in the OpenGL API reference).
         * 
         */
        GLTextureRenderTarget(int textureCount = 1, GLenum format_ = GL_RGBA8) :
        fboHandle(0),
        textures(textureCount),
        format(format_),
        currentTexIndex(0),
        textureArray(NULL) {
            width = 0;
            height = 0;
            currentTexIndex = 0;
        }

        ~GLTextureRenderTarget();

        /*
         * @Override
         */
        void swap(GLTextureRenderTarget& other) {
            //            currentTexture->swap(*other.currentTexture);
            //            swapHelper(fboHandle, other.fboHandle);

            swapHelper(currentTexture, other.currentTexture);
            swapHelper(fboHandle, other.fboHandle);

            swapHelper(width, other.width);
            swapHelper(height, other.height);
            swapHelper(format, other.format);
            swapHelper(sampler, other.sampler);

            swapHelper(textures, other.textures);
            swapHelper(currentTexIndex, other.currentTexIndex);
            swapHelper(textureArray, other.textureArray);
        }

        /**
         * @Override
         */
        bool create() {
            return create(width, height);
        }

        bool create(int width, int height);

        bool create(int width, int height, int textures) {
            this->textures = textures;
            return create(width, height);
        }

        /**
         * @Override
         */
        bool read(void* destPtr) {
            return currentTexture->read(destPtr);
        }

        /**
         * @Override
         */
        bool write(const void* sourcePtr) {
            return currentTexture->write(sourcePtr);
        }

        /**
         * @Override
         */
        int getSize() const {
            return currentTexture->getSize();
        }

        /**
         * @Override
         */
        void destroy() {
            if (textureArray) {
                delete [] textureArray;
                textureArray = NULL;
            }

            if (fboHandle) {
                glDeleteFramebuffers(1, &fboHandle);
                fboHandle = 0;
            }
        }

        bool operator!() const {
            return fboHandle == 0;
        }

        operator bool() const {
            return fboHandle != 0;
        }

        GLuint handle() const {
            assert(fboHandle);
            return fboHandle;
        }

        GLuint textureHandle() {
            return currentTexture->handle();
        }

        void setInitialTextureCount(int textureCount) {
            textures = textureCount;
        }

        void setInitialSize(int width, int height) {
            this->width = width;
            this->height = height;
        }

        void setInitialInternalFormat(GLTexInternalFormat format) {
            this->format = format;
        }

        void setInitialInternalFormat(GLenum format) {
            this->format.set(format);
        }

        void setInitialSampler(
                GLenum minFilter,
                GLenum magFilter,
                GLenum wrapS,
                GLenum wrapT) {
            sampler.set(minFilter, magFilter, wrapS, wrapT);
        }

        void setInitialSampler(GLSampler2D sampler) {
            this->sampler = sampler;
        }

        int getTextureCount() {
            return textures;
        }

        int getHeight() const {
            return height;
        }

        int getWidth() const {
            return width;
        }

        GLTexture2D& getCurrentTexture() {
            return *currentTexture;
        }

        int getCurrentTextureIndex() {
            return currentTexIndex;
        }

        GLTexture2D& getTexture(int index) {
            assert(index < textures && textureArray);

            return textureArray[index];
        }

        void useTexture(int texIndex);

        void use();

        void use(int texIndex) {
            useTexture(texIndex);
            use();
        }

        /**
         * Use default framebuffer.
         */
        void unuse() {
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }

        GLTexInternalFormat getInternalFormat() const {
            return format;
        }

    private:
        bool createTextures();
    };
}











#endif	/* REVGLTEXTURERENDERTARGET_H */

