/* 
 * File:   RevGLAssert.h
 * Author: Revers
 *
 * Created on 27 lipiec 2012, 21:34
 */

#ifndef REVGLASSERT_H
#define	REVGLASSERT_H

#include "RevGLConfig.h"

namespace rev {
    REVGL_API const char* glGetErrorString(GLenum code);
	REVGL_API void glAssert_(const char* function, const char* file, int line);
    REVGL_API void glAssertNoExit_(const char* function, const char* file, int line);
}

#define REVGL_DEBUG_FILE __FILE__
#define REVGL_DEBUG_LINE __LINE__
#define REVGL_DEBUG_FUNCTION __PRETTY_FUNCTION__

#if !defined(NDEBUG) && !defined(REVGL_NO_ASSERT) && !defined(REVGL_ASSERT_NO_EXIT) 

/**
 * if GL error occurs (glGetError() != GL_NO_ERROR), error message
 * is printed out and application exits with -1 error code.
 */
#define glAssert rev::glAssert_(REVGL_DEBUG_FUNCTION, REVGL_DEBUG_FILE, REVGL_DEBUG_LINE)

/**
 * if GL error occurs (glGetError() != GL_NO_ERROR), 
 * error message is printed out.
 */
#define glAssertNoExit rev::glAssertNoExit_(REVGL_DEBUG_FUNCTION, REVGL_DEBUG_FILE, REVGL_DEBUG_LINE)


#elif !defined(NDEBUG) && !defined(REVGL_NO_ASSERT)
/**
 * if GL error occurs (glGetError() != GL_NO_ERROR), 
 * error message is printed out.
 */
#define glAssert rev::glAssertNoExit_(REVGL_DEBUG_FUNCTION, REVGL_DEBUG_FILE, REVGL_DEBUG_LINE)

/**
 * if GL error occurs (glGetError() != GL_NO_ERROR), 
 * error message is printed out.
 */
#define glAssertNoExit rev::glAssertNoExit_(REVGL_DEBUG_FUNCTION, REVGL_DEBUG_FILE, REVGL_DEBUG_LINE)

#else 
/**
 * Empty macro definitions.
 */
#define glAssert ((void)0)
#define glAssertNoExit ((void)0)
#endif

/**
 * Works even if there is NDEBUG flag set. 
 * If GL error occurs (glGetError() != GL_NO_ERROR), error message
 * is printed out and application exits with -1 error code.
 */
#define glAlwaysAssert rev::glAssert_(REVGL_DEBUG_FUNCTION, REVGL_DEBUG_FILE, REVGL_DEBUG_LINE)

/**
 * Works even if there is NDEBUG flag set. 
 * If GL error occurs (glGetError() != GL_NO_ERROR), error message
 * is printed out.
 */
#define glAlwaysAssertNoExit rev::glAssertNoExit_(REVGL_DEBUG_FUNCTION, REVGL_DEBUG_FILE, REVGL_DEBUG_LINE)

#endif	/* REVGLASSERT_H */

