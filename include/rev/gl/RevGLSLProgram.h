#ifndef REVGLSLPROGRAM_H
#define REVGLSLPROGRAM_H

#include <istream>
#include <string>

#include "RevGLConfig.h"

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include "RevColor.h"

namespace rev {
    namespace GLSLShader {

        enum GLSLShaderType {
            VERTEX, FRAGMENT, GEOMETRY,
            TESS_CONTROL, TESS_EVALUATION, NONE
        };
    }
    ;

    class REVGL_API GLSLProgram {
    private:
        GLuint handle;
        bool linked;
        bool verbose;
        GLuint vertexShaderHandle;
        GLuint fragmentShaderHandle;
        GLuint geometryShaderHandle;
        GLuint tessControlShaderHandle;
        GLuint tessEvaluationShaderHandle;
        std::string logString;
        std::string name;

        bool fileExists(const std::string& fileName);

        bool compileShaderGLSLStream(std::istream& in);

        GLSLProgram(const GLSLProgram&);
        GLSLProgram& operator=(const GLSLProgram&);

    public:
        GLSLProgram();
        ~GLSLProgram();

        int getUniformLocation(const char* name);

        bool operator!() const {
            return handle == 0;
        }

        operator bool() const {
            return handle != 0;
        }

        bool isValid() {
            return handle != 0;
        }

        bool compileShaderGLSLString(const char* shaderStr);

        bool compileShaderGLSLFile(const char* filename);

        bool compileShaderFromFile(const char* fileName,
                rev::GLSLShader::GLSLShaderType type);

        bool compileShaderFromString(const std::string& source,
                rev::GLSLShader::GLSLShaderType type);

        /**
         * @return non-zero shader handle or 0 if there was
         * an errror while creating the shader.
         */
        static GLuint staticCompileShaderFromFile(
                const char* fileName,
                rev::GLSLShader::GLSLShaderType type);

        /**
         * @return non-zero shader handle or 0 if there was
         * an errror while creating the shader.
         */
        static REVGL_API GLuint staticCompileShaderFromString(
                const std::string& source,
                rev::GLSLShader::GLSLShaderType type);

        bool link();
        bool validate();
        void use();

        /**
         * Don't use any program (0).
         */
        void unuse();

        std::string log();

        GLuint getHandle();
        bool isLinked();

        void bindAttribLocation(GLuint location, const char* name);
        void bindFragDataLocation(GLuint location, const char* name);

        void setUniform(const char* name, float x, float y, float z);
        void setUniform(const char* name, const glm::vec3& v);
        void setUniform(const char* name, const glm::vec4& v);
        void setUniform(const char* name, const rev::color3& c);
        void setUniform(const char* name, const rev::color4& c);
        void setUniform(const char* name, const glm::mat4& m);
        void setUniform(const char* name, const glm::mat3& m);
        void setUniform(const char* name, float x, float y);
        void setUniform(const char* name, const glm::vec2& v);
        void setUniform(const char* name, float val);
        void setUniform(const char* name, int val);
        void setUniform(const char* name, unsigned int val);
        void setUniform(const char* name, bool val);

        void setUniform(int location, float x, float y, float z);
        void setUniform(int location, float x, float y);
        void setUniform(int location, const glm::vec2& v) {
            this->setUniform(location, v.x, v.y);
        }

        void setUniform(int location, const glm::vec3& v) {
            this->setUniform(location, v.x, v.y, v.z);
        }

        void setUniform(int location, const rev::color3& c) {
            this->setUniform(location, c.r, c.g, c.b);
        }

        void setUniform(int location, const rev::color4& c);
        void setUniform(int location, const glm::vec4& v);
        void setUniform(int location, const glm::mat4& m);
        void setUniform(int location, const glm::mat3& m);
        void setUniform(int location, float val);
        void setUniform(int location, int val);
        void setUniform(int location, unsigned int val);
        void setUniform(int location, bool val);

        /**
         * shaderType
         *      GL_VERTEX_SHADER
         *      GL_TESS_CONTROL_SHADER 
         *      GL_TESS_EVALUATION_SHADER
         *      GL_GEOMETRY_SHADER 
         *      GL_FRAGMENT_SHADER
         *
         */
        GLuint getSubroutineIndex(const char* name, GLenum shaderType) {
            return glGetSubroutineIndex(handle, shaderType, name);
        }

        /**
         * shaderType
         *      GL_VERTEX_SHADER
         *      GL_TESS_CONTROL_SHADER 
         *      GL_TESS_EVALUATION_SHADER
         *      GL_GEOMETRY_SHADER 
         *      GL_FRAGMENT_SHADER
         *
         */
        void setSubroutine(GLuint subroutineIndex, GLenum shaderType) {
            glUniformSubroutinesuiv(shaderType, 1, &subroutineIndex);
        }

        /**
         * shaderType
         *      GL_VERTEX_SHADER
         *      GL_TESS_CONTROL_SHADER 
         *      GL_TESS_EVALUATION_SHADER
         *      GL_GEOMETRY_SHADER 
         *      GL_FRAGMENT_SHADER
         *
         */
        void setSubroutine(const char* name, GLenum shaderType);

        void printActiveUniforms();
        void printActiveAttribs();

        bool isVerbose() const {
            return verbose;
        }

        void setVerbose(bool verbose) {
            this->verbose = verbose;
        }

        GLuint getVertexShader() const {
            return vertexShaderHandle;
        }

        GLuint getFragmentShader() const {
            return fragmentShaderHandle;
        }

        GLuint getGeometryShader() const {
            return geometryShaderHandle;
        }

        GLuint getTessControlShader() const {
            return tessControlShaderHandle;
        }

        GLuint getTessEvaluationShader() const {
            return tessEvaluationShaderHandle;
        }

        /**
         * WARNING: Do not set shader if there is already
         * compiled that type of shader. This could lead to
         * a memory leaks.
         *
         * @return true if succeed, false otherwise.
         */
        bool setVertexShader(GLuint vertexShaderHandle);

        /**
         * WARNING: Do not set shader if there is already
         * compiled that type of shader. This could lead to
         * a memory leaks.
         *
         * @return true if succeed, false otherwise.
         */
        bool setFragmentShader(GLuint fragmentShader);

        /**
         * WARNING: Do not set shader if there is already
         * compiled that type of shader. This could lead to
         * a memory leaks.
         *
         * @return true if succeed, false otherwise.
         */
        bool setGeometryShader(GLuint geometryShader);

        /**
         * WARNING: Do not set shader if there is already
         * compiled that type of shader. This could lead to
         * a memory leaks.
         *
         * @return true if succeed, false otherwise.
         */
        bool setTessControlShader(GLuint tessControlShader);

        /**
         * WARNING: Do not set shader if there is already
         * compiled that type of shader. This could lead to
         * a memory leaks.
         *
         * @return true if succeed, false otherwise.
         */
        bool setTessEvaluationShader(GLuint tessEvaluationShader);

    private:
        bool createProgramIfNeeded();
    };
}

#endif // REVGLSLPROGRAM_H
