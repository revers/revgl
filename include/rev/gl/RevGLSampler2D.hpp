/* 
 * File:   RevGLSampler2D.hpp
 * Author: Revers
 *
 * Created on 5 lipiec 2012, 18:51
 */

#ifndef REVGLSAMPLER2D_HPP
#define	REVGLSAMPLER2D_HPP

#include "RevGLConfig.h"
#include <GL/gl.h>

namespace rev {

    /**
     * rev::GLSampler2D
     * 
     * @see <a href="http://www.opengl.org/sdk/docs/man4/xhtml/glTexParameter.xml">glTexParameter</a>
     */
    struct REVGL_API GLSampler2D {
        /**
         * Possible values for GL_TEXTURE_MIN_FILTER are:
         *      GL_NEAREST
         *      GL_LINEAR
         *      GL_NEAREST_MIPMAP_NEAREST
         *      GL_LINEAR_MIPMAP_NEAREST
         *      GL_NEAREST_MIPMAP_LINEAR
         *      GL_LINEAR_MIPMAP_LINEAR
         */
        GLenum minFilter;

        /**
         * Possible values for GL_TEXTURE_MAG_FILTER are:
         *      GL_NEAREST
         *      GL_LINEAR. 
         *      GL_NEAREST
         */
        GLenum magFilter;

        /**
         * Possible values for GL_TEXTURE_WRAP_S are:
         *      GL_CLAMP_TO_EDGE
         *      GL_CLAMP_TO_BORDER
         *      GL_MIRRORED_REPEAT
         *      GL_REPEAT.
         */
        GLenum wrapS;

        /**
         * Possible values for GL_TEXTURE_WRAP_T are:
         *      GL_CLAMP_TO_EDGE
         *      GL_CLAMP_TO_BORDER
         *      GL_MIRRORED_REPEAT
         *      GL_REPEAT.
         */
        GLenum wrapT;

        GLSampler2D(GLenum minFilter_ = GL_LINEAR,
                GLenum magFilter_ = GL_LINEAR,
                GLenum wrapS_ = GL_REPEAT,
                GLenum wrapT_ = GL_REPEAT) :
        minFilter(minFilter_),
        magFilter(magFilter_),
        wrapS(wrapS_),
        wrapT(wrapT_) {
        }

        void setWrap(GLenum wrapS, GLenum wrapT) {
            this->wrapS = wrapS;
            this->wrapT = wrapT;
        }

        void setFilter(GLenum minFilter, GLenum magFilter) {
            this->minFilter = minFilter;
            this->magFilter = magFilter;
        }

        void set(GLenum minFilter, GLenum magFilter, GLenum wrapS, GLenum wrapT) {
            this->minFilter = minFilter;
            this->magFilter = magFilter;
            this->wrapS = wrapS;
            this->wrapT = wrapT;
        }
    };
} // end namespace rev

#endif	/* REVGLSAMPLER2D_HPP */

