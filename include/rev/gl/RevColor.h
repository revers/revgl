/*
 * RevColor.h
 *
 *  Created on: 04-02-2013
 *      Author: Revers
 */

#ifndef REVCOLOR_H_
#define REVCOLOR_H_

namespace rev {

    namespace detail {
        template<typename T>
        struct tcolor3 {
            typedef tcolor3<T> color3_t;

            T r;
            T g;
            T b;

            tcolor3(T r, T g, T b) :
                    r(r), g(g), b(b) {
            }

            tcolor3() :
                    r(0), g(0), b(0) {
            }

            T& operator[](int index) {
                return *(&r + index);
            }

            const T& operator[](int index) const {
                return *(&r + index);
            }

            friend color3_t operator+(const color3_t& c1, const color3_t& c2) {
                return color3_t(c1.r + c2.r, c1.g + c2.g, c1.b + c2.b);
            }

            friend color3_t operator*(const color3_t& c, const T& scalar) {
                return color3_t(c.r * scalar, c.g * scalar, c.b * scalar);
            }

            friend color3_t operator*(const T& scalar, const color3_t& c) {
                return color3_t(c.r * scalar, c.g * scalar, c.b * scalar);
            }
        };

        template<typename T>
        struct tcolor4 {
            typedef tcolor4<T> color4_t;

            T r;
            T g;
            T b;
            T a;

            tcolor4(T r, T g, T b, T a) :
                    r(r), g(g), b(b), a(a) {
            }

            tcolor4() :
                    r(0), g(0), b(0), a(0) {
            }

            T& operator[](int index) {
                return *(&r + index);
            }

            const T& operator[](int index) const {
                return *(&r + index);
            }

            friend color4_t operator+(const color4_t& c1, const color4_t& c2) {
                return color4_t(c1.r + c2.r, c1.g + c2.g, c1.b + c2.b, c1.a + c2.a);
            }

            friend color4_t operator*(const color4_t& c, const T& scalar) {
                return color4_t(c.r * scalar, c.g * scalar, c.b * scalar,
                        c.a * scalar);
            }

            friend color4_t operator*(const T& scalar, const color4_t& c) {
                return color4_t(c.r * scalar, c.g * scalar, c.b * scalar,
                        c.a * scalar);
            }
        };
    }

    template<typename T>
    T* value_ptr(detail::tcolor3<T>& c) {
        return &(c.r);
    }
    template<typename T>
    T const* value_ptr(const detail::tcolor3<T>& c) {
        return &(c.r);
    }

    template<typename T>
    T* value_ptr(detail::tcolor4<T>& c) {
        return &(c.r);
    }

    template<typename T>
    T const* value_ptr(const detail::tcolor4<T>& c) {
        return &(c.r);
    }

    typedef rev::detail::tcolor3<float> color3;
    typedef rev::detail::tcolor4<float> color4;
}

#endif /* REVCOLOR_H_ */
