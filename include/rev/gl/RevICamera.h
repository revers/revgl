/*
 * RevICamera.h
 *
 *  Created on: 26-11-2012
 *      Author: Revers
 */

#ifndef REVICAMERA_H_
#define REVICAMERA_H_

#include "RevGLConfig.h"
#include <glm/glm.hpp>

namespace rev {

    class ICamera {
    public:
        virtual ~ICamera() {
        }

        virtual const glm::vec3& getPosition() const = 0;
        virtual const glm::vec3& getDirection() const = 0;
        virtual const glm::mat4& getViewMatrix() const = 0;
    };
}

#endif /* REVICAMERA_H_ */
