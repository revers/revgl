/* 
 * File:   RevTexture2D.h
 * Author: Revers
 *
 * Created on 3 czerwiec 2012, 13:01
 */

#ifndef REVGLTEXTURE2D_H
#define	REVGLTEXTURE2D_H

#include "RevGLConfig.h"
#include <GL/gl.h>

#include "RevGLSampler2D.hpp"
#include "RevGLTexUploadFormat.hpp"
#include "RevGLTexInternalFormat.h"

#include <rev/common/RevIBuffer.hpp>
#include <rev/common/RevAssert.h>

namespace rev {

    class GLTexture2D;

    class REVGL_API GLTexture2D : public IBuffer<GLTexture2D> {
        int width;
        int height;
        GLuint tex;
        int sizeInBytes;

        GLTexInternalFormat internalFormat;
        GLTexUploadFormat uploadFormat;
        GLSampler2D sampler;

        GLenum textureType;

        GLTexture2D(const GLTexture2D&);
        GLTexture2D& operator=(const GLTexture2D&);

        void updateSampler();
    public:

        /**
         * Most common formats are:
         *      GL_RGB8
         *      GL_RGBA8
         *      GL_RGB16F
         *      GL_RGBA16F
         *      GL_RGB32F
         *      GL_RGBA32F
         * but there are many more (described in the OpenGL API reference).
         * 
         */
        GLTexture2D(GLenum format = GL_RGBA8,
                GLenum minFilter = GL_LINEAR,
                GLenum magFilter = GL_LINEAR,
                GLenum wrapS = GL_REPEAT,
                GLenum wrapT = GL_REPEAT) :
        internalFormat(format),
        uploadFormat(GL_RGB, GL_UNSIGNED_BYTE),
        sampler(minFilter, magFilter, wrapS, wrapT) {
            textureType = GL_TEXTURE_2D;
            sizeInBytes = 0;
            width = 0;
            height = 0;
        }

        ~GLTexture2D() {
            if (tex) {
                glDeleteTextures(1, &tex);
            }
        }

        /*
         * @Override
         */
        void swap(GLTexture2D& other) {
            swapHelper(width, other.width);
            swapHelper(height, other.height);
            swapHelper(tex, other.tex);
            swapHelper(sizeInBytes, other.sizeInBytes);
            swapHelper(internalFormat, other.internalFormat);
            swapHelper(uploadFormat, other.uploadFormat);
            swapHelper(sampler, other.sampler);
            swapHelper(textureType, other.textureType);
        }

        bool load(const char* filename);

        bool create(int width, int height);

        GLuint handle() const {
            assert(tex);
            return tex;
        }

        bool isInitialized() {
            return tex != 0;
        }

        void bind();

        void unbind() {
            glBindTexture(textureType, 0);
        }

        void bindToUnit(GLenum textureUnit);

        void unbindFromUnit(GLenum textureUnit);

        GLTexInternalFormat getInternalFormat() const {
            return internalFormat;
        }

        GLTexUploadFormat getUploadFormat() const {
            return uploadFormat;
        }

        GLSampler2D getSampler() const {
            return sampler;
        }

        GLenum getTextureType() const {
            return textureType;
        }

        int getHeight() const {
            return height;
        }

        int getWidth() const {
            return width;
        }

        GLenum getMagFilter() const {
            return sampler.magFilter;
        }

        GLenum getMinFilter() const {
            return sampler.minFilter;
        }

        GLenum getWrapS() const {
            return sampler.wrapS;
        }

        GLenum getWrapT() const {
            return sampler.wrapT;
        }

        GLenum getChannelFormat() const {
            return uploadFormat.channelFormat;
        }

        GLenum getChannelType() const {
            return uploadFormat.channelType;
        }

        void setInitialSampler(GLSampler2D sampler) {
            this->sampler = sampler;
        }

        void setInitialFilters(GLenum minFilter, GLenum magFilter) {
            sampler.minFilter = minFilter;
            sampler.magFilter = magFilter;
        }

        void setInitialWrap(GLenum wrapS, GLenum wrapT) {
            sampler.wrapS = wrapS;
            sampler.wrapT = wrapT;
        }

        void setInitialInternalFormat(GLTexInternalFormat format) {
            this->internalFormat = format;
        }

        void setInitialInternalFormat(GLenum format) {
            this->internalFormat.set(format);
        }

        void setInitialUploadFormat(GLTexUploadFormat format) {
            this->uploadFormat = format;
        }

        void setInitialUploadFormat(GLenum channelFormat, GLenum channelType) {
            uploadFormat.channelFormat = channelFormat;
            uploadFormat.channelType = channelType;
        }

        void setInitialSize(int width, int height) {
            this->width = width;
            this->height = height;
        }

        void setInitialTextureType(GLenum textureType) {
            this->textureType = textureType;
        }

        void setInitialSampler(
                GLenum minFilter,
                GLenum magFilter,
                GLenum wrapS,
                GLenum wrapT) {
            sampler.set(minFilter, magFilter, wrapS, wrapT);
        }

        void setSampler(GLSampler2D sampler);

        void setSampler(
                GLenum minFilter,
                GLenum magFilter,
                GLenum wrapS,
                GLenum wrapT);

        void setFilter(GLenum minFilter, GLenum magFilter);

        void setWrap(GLenum wrapS, GLenum wrapT);

        /**
         * @Override
         */
        bool create() {
            return create(width, height);
        }

        /**
         * @Override
         */
        bool read(void* destPtr) {
            assert(tex);
            return true;
        }

        /**
         * @Override
         */
        bool write(const void* sourcePtr) {
            assert(tex);
            return true;
        }

        /**
         * @Override
         */
        int getSize() const {
            assert(tex);
            return sizeInBytes;
        }

        /**
         * @Override
         */
        void destroy() {
            if (tex) {
                glDeleteTextures(1, &tex);
                tex = 0;
                sizeInBytes = 0;
                width = 0;
                height = 0;
            }
        }

        bool operator!() const {
            return tex == 0;
        }

        operator bool() const {
            return tex != 0;
        }

        //----------------------------------------------------------
        class Builder;
        friend class GLTexture2D::Builder;

        class Builder {
            GLTexture2D* texture;
        public:

            Builder(GLTexture2D* texture_) : texture(texture_) {
            }

            GLTexture2D& size(int width, int height) {
                texture->setInitialSize(width, height);
                return *texture;
            }

            GLTexture2D& uploadFormat(GLenum channelFormat, GLenum channelType) {
                texture->setInitialUploadFormat(channelFormat, channelType);
                return *texture;
            }

            GLTexture2D& filters(GLenum minFilter, GLenum magFilter) {
                texture->setInitialFilters(minFilter, magFilter);
                return *texture;
            }

            GLTexture2D& wrap(GLenum wrapS, GLenum wrapT) {
                texture->setInitialWrap(wrapS, wrapT);
                return *texture;
            }

            GLTexture2D& uploadFormat(GLTexUploadFormat format_) {
                texture->setInitialUploadFormat(format_);
                return *texture;
            }

            GLTexture2D& sampler(GLSampler2D sampler_) {
                texture->setInitialSampler(sampler_);
                return *texture;
            }

            GLTexture2D& sampler(GLenum minFilter, GLenum magFilter,
                    GLenum wrapS, GLenum wrapT) {

                texture->setInitialSampler(minFilter, magFilter, wrapS, wrapT);

                return *texture;
            }

            GLTexture2D& textureType(GLenum textureType_) {
                texture->setInitialTextureType(textureType_);
                return *texture;
            }

            bool create() {
                return texture->create();
            }
        };

        Builder builder() {
            return Builder(this);
        }


        //==========================================================
    };
}


#endif	/* REVGLTEXTURE2D_H */

