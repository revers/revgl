#ifndef REVGL_CONFIG_H
#define	REVGL_CONFIG_H

#define REVGL_VERSION_MAJOR 0
#define REVGL_VERSION_MINOR 1 

#ifndef REVGL_STATIC
/* #undef REVGL_STATIC */
#endif

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(_WIN64)
#define REVGL_WINDOWS
#endif

#if defined(REVGL_WINDOWS)
#   define REVGL_CDECL_CALL    __cdecl
#   define REVGL_STD_CALL      __stdcall
#   define REVGL_CALL          REVGL_CDECL_CALL
#   define REVGL_EXPORT_API    __declspec(dllexport)
#   define REVGL_IMPORT_API    __declspec(dllimport)
#else
#   define REVGL_CDECL_CALL
#   define REVGL_STD_CALL
#   define REVGL_CALL
#   define REVGL_EXPORT_API
#   define REVGL_IMPORT_API
#endif

#if defined REVGL_EXPORTS
#   define REVGL_API REVGL_EXPORT_API
#elif defined REVGL_STATIC
#   define REVGL_API
#   if defined(_MSC_VER) && !defined(REVGL_NO_LIB_PRAGMA)
#       ifdef _WIN64
#           pragma comment(lib, "RevGLStatic64")
#       else
#           pragma comment(lib, "RevGLStatic")
#       endif
#   endif
#else
#   define REVGL_API REVGL_IMPORT_API
#   if defined(_MSC_VER) && !defined(REVGL_NO_LIB_PRAGMA)
#       ifdef _WIN64
#           pragma comment(lib, "RevGL64")
#       else
#           pragma comment(lib, "RevGL")
#       endif
#   endif
#endif

typedef unsigned int GLenum;

#endif /* REVGL_CONFIG_H */
