/* 
 * File:   RevGLQuadVT.h
 * Author: Revers
 *
 * Created on 3 czerwiec 2012, 13:03
 */

#ifndef REVVBOQUADVT_H
#define	REVVBOQUADVT_H

#include "RevGLConfig.h"

namespace rev {

    class REVGL_API GLQuadVT {
        unsigned int vaoHandle[1];
        unsigned int handle[2];

        float xMin;
        float xMax;
        float yMin;
        float yMax;

    public:
        GLQuadVT(float length = 1.0f);
        GLQuadVT(float xMin, float xMax, float yMin, float yMax);

        ~GLQuadVT();

        void render();

        bool create(float xMin = -1.0f, float xMax = 1.0f, 
            float yMin = -1.0f, float yMax = 1.0f);

    private:
        bool generate();

        void initVAO(int contextIndex);
    };

}
#endif	/* REVVBOQUADVT_H */

