/* 
 * File:   vboaxes.h
 * Author: Revers
 *
 * Created on 29 luty 2012, 15:51
 */

#ifndef REVGLAXES_H
#define	REVGLAXES_H

#include "RevGLConfig.h"
#include "RevGLSLProgram.h"

#include <glm/glm.hpp>

namespace rev {

    class REVGL_API GLAxes {
    private:
        GLSLProgram axesProg;
        unsigned int vaoHandle;
        float length;

    public:
        GLAxes(float length_ = 20.0f);

        bool init();

        void render(const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix);
    };
}
#endif	/* REVGLAXES_H */

