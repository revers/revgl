/* 
 * File:   GLTexUploadFormat.hpp
 * Author: Revers
 *
 * Created on 5 lipiec 2012, 19:16
 */

#ifndef REVGLTEXUPLOADFORMAT_HPP
#define	REVGLTEXUPLOADFORMAT_HPP

#include "RevGLConfig.h"
#include <GL/gl3.h>

namespace rev {

    /**
     * rev::GLTexUploadFormat
     * 
     * @see <a href="http://www.opengl.org/sdk/docs/man4/xhtml/glTexImage2D.xml">glTexImage2D</a>
     */
    struct REVGL_API GLTexUploadFormat {
        /**
         * Most common channel formats are: 
         *      GL_RED
         *      GL_RG
         *      GL_RGB
         *      GL_RGBA
         *      GL_DEPTH_COMPONENT
         *      GL_DEPTH_STENCIL
         * but there are many more (described in official OpenGL documentation).
         */
        GLenum channelFormat;

        /**
         * Most common channel types are: 
         *      GL_UNSIGNED_BYTE
         *      GL_FLOAT
         *      GL_BYTE
         *      GL_UNSIGNED_SHORT
         *      GL_SHORT
         *      GL_UNSIGNED_INT
         *      GL_INT
         * but there are many more (described in official OpenGL documentation).
         */
        GLenum channelType;

        GLTexUploadFormat(GLenum channelFormat_ = GL_RGB,
                GLenum channelType_ = GL_UNSIGNED_BYTE) :
        channelFormat(channelFormat_),
        channelType(channelType_) {
        }

        void set(GLenum channelFormat, GLenum channelType) {
            this->channelFormat = channelFormat;
            this->channelType = channelType;
        }

        int getSize() {
            return getChannelFormatSize(channelFormat)
                    * getChannelTypeSize(channelType);
        }

        static int getChannelFormatSize(GLenum channelFormat) {
            switch (channelFormat) {
                case GL_RED: return 1;
                case GL_RG: return 2;
                case GL_RGB: return 3;
                case GL_BGR: return 3;
                case GL_RGBA: return 4;
                case GL_BGRA: return 4;
                case GL_DEPTH_COMPONENT: return 1;
                case GL_DEPTH_STENCIL: return 2;
                default: return 0;
            }
        }

        static int getChannelTypeSize(GLenum channelType) {
            switch (channelType) {
                case GL_BYTE: return sizeof (GLbyte);
                case GL_UNSIGNED_BYTE: return sizeof (GLubyte);
                case GL_SHORT: return sizeof (GLshort);
                case GL_UNSIGNED_SHORT: return sizeof (GLushort);
                case GL_INT: return sizeof (GLint);
                case GL_UNSIGNED_INT: return sizeof (GLuint);
                case GL_FLOAT: return sizeof (GLfloat);
                case GL_DOUBLE: return sizeof (GLdouble);
                case GL_2_BYTES: return 2;
                case GL_3_BYTES: return 3;
                case GL_4_BYTES: return 4;
                case GL_RGB: return 3;
                case GL_RGBA: return 4;
                default: return 0;
            }
        }
    };

} // end namespace rev


#endif	/* REVGLTEXUPLOADFORMAT_HPP */

