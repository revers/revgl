#ifndef REVGLUTILS_H
#define REVGLUTILS_H

#include "RevGLConfig.h"
#include <string>

namespace rev {

    class REVGL_API GLUtil {
    public:
        GLUtil();

        static int checkForOpenGLError(const char *, int);
        static void dumpGLInfo(bool dumpExtensions = false);
        static std::string getGLInfo(bool dumpExtensions = false);

        static int getSize(int glSizeType);
        
    };
}
#endif // REVGLUTILS_H
